function Board(width, height) {
    this.width = width;
    this.height = height;
    this.matrix = [];
}

Board.prototype.generateMap = function() {
    for (let i = 0; i < this.width; i++) {
        this.matrix.push([]);
        for (let j = 0; j < this.width; j++) {
            this.matrix[i][j] = new Cell(i, j);
        }
    }
};
