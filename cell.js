function Cell(x, y) {
    this.x = x;
    this.y = y;
    this.visited = false;
    this.neighbors = [];
    this.havePathTo = [];
}

/**
 * top    x, y-1
 * bottom x, y+1
 * right  x+1, y
 * left   x-1, y
 */

Cell.prototype.findNeighbor = function() {
    /* left*/
    if (this.x > 0) {
        this.neighbors.push(board.matrix[this.x-1][this.y]);
    }
    /* right*/
    if (this.x != board.width - 1) {
        this.neighbors.push(board.matrix[this.x+1][this.y]);
    }
    /* top*/
    if (this.y > 0) {
        this.neighbors.push(board.matrix[this.x][this.y-1]);
    }
    /* bottom*/
    if (board.height) {
        this.neighbors.push(board.matrix[this.x][this.y+1]);
    }

    this.neighbors = this.neighbors.filter(neighbor => neighbor);
};

Cell.prototype.findPath = function () {
    this.visited = true;
    let unvisitedNeighbors = [];
    for (let neighbor in this.neighbors) {
        if (!this.neighbors[neighbor].visited) {
            unvisitedNeighbors.push(this.neighbors[neighbor]);
        }
    }
    if (unvisitedNeighbors.length) {
        let randomNeighbor = unvisitedNeighbors[Math.ceil(Math.random() * unvisitedNeighbors.length-1)];
        this.havePathTo.push(randomNeighbor);
        randomNeighbor.havePathTo.push(this);

        return randomNeighbor;
    }

    return null;
};

