function Person(x, y) {
    this.x = x;
    this.y = y;

    window.addEventListener('keydown', event => {
        const key = event.key;
        const paths = board.matrix[this.x][this.y].havePathTo;
        switch (key) {
            case 'ArrowRight':
                if (this.x+1 < board.width && paths.includes(board.matrix[this.x+1][this.y])) {
                    this.x++;
                }
                break;
            case 'ArrowLeft':
                if (this.x-1 >= 0 && paths.includes(board.matrix[this.x-1][this.y])) {
                    this.x--;
                }
                break;
            case 'ArrowUp':
                if (this.y-1 >= 0 && paths.includes(board.matrix[this.x][this.y-1])) {
                    this.y--;
                }
                break;
            case 'ArrowDown':
                if (this.y+1 < board.height && paths.includes(board.matrix[this.x][this.y+1])) {
                    this.y++;
                }
        }
    });

}