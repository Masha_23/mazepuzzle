let rows = document.getElementById('rows').value;
let columns = document.getElementById('columns').value;

const rectSize = 50;
const canvasWidth = rows * rectSize;
const canvasHeight = columns * rectSize;

/* get matrix */
const board = new Board(rows, columns);

/* create person obj*/
const person = new Person(0, 0);

function customSetup() {
	/* get board cells */
	board.generateMap();
	for (let i = 0; i < rows; i++) {
		for (let j = 0; j < columns; j++) {
			board.matrix[i][j].findNeighbor();
		}
	}

	/* generate maze path*/
	let cursor = board.matrix[0][0];
	let visitedCount = 0;
	while(true) {
		loop1: {
			cursor = cursor.findPath();
			if (!cursor) {
				for (const arr of board.matrix) {
					for (const cell of arr) {
						const nbr = cell.neighbors.find(nbr => nbr.visited);
						if (!cell.visited) {
							if (nbr) {
								cursor = cell;
								cursor.havePathTo.push(nbr);
								nbr.havePathTo.push(cursor);

								break loop1;
							}
						}
					}
				}
				if (!cursor) {
					break;
				}
			}
		}
	}
	createCanvas(canvasWidth, canvasHeight);
}

function setup() {
	customSetup();
}

function draw() {
	/* draw matrix */
	stroke('#000');
	strokeWeight(2);
	for (let x = 0; x <= canvasWidth; x+=rectSize) {
		for (let y = 0; y <= canvasHeight; y+=rectSize){
			fill('#fff');
			rect(x, y, rectSize, rectSize);
		}
	}

	/* draw maze path */
	stroke('#fff');
	for (let i = 0; i < board.width; i++) {
		for (let j = 0; j < board.height; j++){
			const currentCell = board.matrix[i][j];
			currentCell.havePathTo.map(path => {
				if (path.x > currentCell.x) {
					line(path.x*rectSize, path.y*rectSize, path.x*rectSize, (path.y+1)*rectSize);
				}
				else if (path.x < currentCell.x) {
					line(currentCell.x*rectSize, path.y*rectSize, currentCell.x*rectSize, (path.y+1)*rectSize);
				}
				else if (path.y > currentCell.y) {
					line(path.x*rectSize, path.y*rectSize, (path.x+1)*rectSize, path.y*rectSize);
				}
				else if (path.y < currentCell.y) {
					line(currentCell.x*rectSize, currentCell.y*rectSize, (currentCell.x+1)*rectSize, currentCell.y*rectSize);
				}
			});
		}
	}

	/* draw person*/
	fill('#0f0');
	rect(person.x * rectSize + 5, person.y* rectSize + 5, rectSize-10, rectSize-10);
}




